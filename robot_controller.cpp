#include <umrob/robot_controller.h>

#include <fmt/format.h>
#include <fmt/ostream.h>

namespace umrob {

RobotController::RobotController(Robot& robot, double time_step)
    : robot_{robot}, time_step_{time_step} {
    // TODO: implement
}

void RobotController::addJointsPositionConstraint() {

//les contraintes des positions
      qp.addConstraint(box(robot.jointslimits.min_position,robot.JointsData.position,Robot.jointslimits.max_position));
//mettre en à jour le vecteur de position
      robot.updatePositionVector.setRandom()      ;
      robot.updatePositionVector = robot.updatePositionVector.cwiseAbs();                                                            // TODO: implement
}

void RobotController::addJointsVelocityConstraint() {
    // TODO: implement
    qp.addConstraint(box(-robot.jointslimits.max_velocity,robot.JointsData.velocity,Robot.jointslimits.max_velocity));

    robot.updatevelocityVector.setRandom()      ;
      robot.updatevelocityVector = robot.updatevelocityVector.cwiseAbs();  
}

void RobotController::addControlPointVelocityConstraint([
    [maybe_unused]] const std::string& control_point) {
    // TODO: implement
}

void RobotController::addControlPointPositionTask(
    [[maybe_unused]] const std::string& control_point,
    [[maybe_unused]] double weight, [[maybe_unused]] double gain) {
    // TODO: implement
}

void RobotController::addControlPointVelocityTask(
    [[maybe_unused]] const std::string& control_point,
    [[maybe_unused]] double weight) {
    // TODO: implement
}

void RobotController::setupTask([
    [maybe_unused]] const std::string& control_point) {
    // TODO: implement
}

void RobotController::reset() {
    // TODO: implement
}

bool RobotController::update() {
    if (not solver_) {
        throw std::runtime_error("You must setup you control problem before "
                                 "calling RobotController::update()");
    }

    // Step 1: update task Jacobians
    // TODO: implement

    // Step 2: solve the problem
    // TODO: implement

    // Step 3: update the joints command vector using the solution
    // TODO: implement

    // Step 4: update the control points commands
    // TODO: implement

    return true;
}

void RobotController::print() const {
    fmt::print("{}", qp_);
}

void RobotController::resetSolver() {
    solver_ = nullptr;
    solver_ = std::make_unique<cvx::osqp::OSQPSolver>(qp_);
    solver_->setAlpha(1.);
    solver_->setEpsAbs(1e-6);
    solver_->setEpsRel(1e-6);
}

} // namespace umrob