if(ENABLE_TESTING)
    # Compile Catch2 main function separately to share it across tests and speed up subsequent builds
    add_library(test_main STATIC common/main.cpp)
    target_link_libraries(test_main PUBLIC CONAN_PKG::catch2)

    # List all the source files composing the tests
    set(controller_test_FILES 
        controller/controller.cpp
    )

    add_executable(controller_test ${controller_test_FILES})

    target_link_libraries(controller_test PUBLIC test_main robot_control)

    add_test(NAME controller-test COMMAND controller_test)

    set(robot_test_FILES 
        robot/robot.cpp
    )

    add_executable(robot_test ${robot_test_FILES})

    target_link_libraries(robot_test PUBLIC test_main robot_control)

    add_test(NAME robot-test COMMAND robot_test)
endif()