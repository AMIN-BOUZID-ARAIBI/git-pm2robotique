#include <umrob/robot.h>
#include <umrob/robot_model.h>
#include <umrob/robot_controller.h>
#include <fmt/format.h>

#include <limits>

namespace umrob {

Robot::Robot(RobotModel& model)
    : joints_{model.degreesOfFreedom()}, model_{model} {
    // TODO: implement
    auto robot = umrob::Robot(model);
    setJointsLimitsFromModel();
}

Robot::Joints& Robot::joints() {
    return joints_;
}

const Robot::Joints& Robot::joints() const {
    return joints_;
}

Robot::ControlPoint& Robot::controlPoint(const std::string& name) {
    // TODO: implement
      auto it = control_points_.find(name);
    if(it != control_points_.end()) {
        return it->second;
    }
    else if(model_.hasLink(name)) {
        return control_points_[name];
    }
    else {
        throw std::runtime_error("Requested control doesn't match a link in the model");
    }
    return control_points_.at(name); // To silence warning, not actual implementation
}

const Robot::ControlPoint& Robot::controlPoint(const std::string& name) const {
    return control_points_.at(name);
}

RobotModel& Robot::model() {
    return model_;
}
const RobotModel& Robot::model() const {
    return model_;
}

void Robot::updatePositionVectors() {
    // TODO: implement
    auto it = control_points_.begin();
    for(;it!=control_points_.end();it++){}
     //Robot::ControlPointData().position_vector=control_point.position.translation();
   //updatePositionVector(control_points_.at(it->first));
 control_points_.at(it->first);
}
}

void Robot::updatePositionVector(Robot::ControlPointData& cp) {
    cp.position_vector.head<3>() = cp.position.translation();

    auto angle_axis = Eigen::AngleAxisd(cp.position.linear());
    cp.position_vector.tail<3>() = angle_axis.angle() * angle_axis.axis();
}

void Robot::setStateFromModel() {
   for(size_t i=0;i<=12;i++){
   const std::vector<std::string> ss = model().jointNameByIndex(i);
  
   
    
   Robot::joints().state.position=model_.jointsPosition(ss);
   control_points_.at(ss[i])= Robot::joints().state.position(i);
   
   updatePositionVectors();
   } 
    
   
}

void Robot::updateModelFromState() {
    //for(i=0;i<=6;i++)
       //model.joitNameByIndex([i])=joints(name).state.position;
       
       for(size_t i=0;i<=12;i++)
       {
        const std::vector<std::string> ss = model_.jointNameByIndex(i);
        model_.jointsPosition[ss]=Robot::joints().state.position[i]

       }

}

void Robot::update() {
    // TODO: implement
   
    setStateFromModel();
    updateModelFromState();
     

}

void Robot::setJointsLimitsFromModel() {
    // TODO: implement
     for(size_t i=0;i<=12;i++)
       {
        const std::vector<std::string> ss = model_.jointNameByIndex(i);
    
   
     Robot::joints().limits.min_position=model_.jointLimits(ss);
   }
}

Robot::JointsData::JointsData(size_t dofs)
    : position{Eigen::VectorXd::Zero(dofs)},
      velocity{Eigen::VectorXd::Zero(dofs)} {
}

Robot::JointsLimits::JointsLimits(size_t dofs)
    : min_position{Eigen::VectorXd::Constant(
          dofs, -std::numeric_limits<double>::infinity())},
      max_position{Eigen::VectorXd::Constant(
          dofs, std::numeric_limits<double>::infinity())},
      max_velocity{Eigen::VectorXd::Constant(
          dofs, std::numeric_limits<double>::infinity())} {
}

Robot::Joints::Joints(size_t dofs) : state{dofs}, command{dofs}, limits{dofs} {
}

Robot::ControlPointData::ControlPointData()
    : position{Eigen::Affine3d::Identity()},
      position_vector{Eigen::Vector6d::Zero()},
      velocity{Eigen::Vector6d::Zero()} {
}

Robot::ControlPointLimits::ControlPointLimits()
    : max_velocity{
          Eigen::Vector6d::Constant(std::numeric_limits<double>::infinity())} {
}

} // namespace umrob